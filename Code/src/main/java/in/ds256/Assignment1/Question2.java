package in.ds256.Assignment1;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import com.martinkl.warc.WARCWritable;
import com.martinkl.warc.mapreduce.WARCInputFormat;
/**
 * DS-256 Assignment 1
 * Code for question 2(Percentage of pages whose title contains one or more of the top 5 keywords)
 * Note that this Assignment has a new userArgs input where the user can define any required arguments in CSV format.
 */
public class Question2 
{
    public static void main( String[] args )
    {
	SparkConf conf = new SparkConf().setAppName("KeywordinTitle");
        JavaSparkContext sc = new JavaSparkContext(conf);
        String inputPath = args[0];
	String outputPath = args[1];
	String userArgs   = args[2];
	/**
	 * Code goes here
	 */
        sc.stop();
    }
}
